package com.seop.studycafemanager1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyCafeManager1Application {

    public static void main(String[] args) {
        SpringApplication.run(StudyCafeManager1Application.class, args);
    }

}
