package com.seop.studycafemanager1.repository;

import com.seop.studycafemanager1.entity.StudyCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudyCustomerRepository extends JpaRepository<StudyCustomer, Long> {
}
