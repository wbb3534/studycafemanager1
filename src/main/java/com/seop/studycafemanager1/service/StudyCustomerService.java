package com.seop.studycafemanager1.service;

import com.seop.studycafemanager1.entity.StudyCustomer;
import com.seop.studycafemanager1.model.CustomerItem;
import com.seop.studycafemanager1.model.CustomerRequest;
import com.seop.studycafemanager1.model.CustomerUseTimeUpdateRequest;
import com.seop.studycafemanager1.repository.StudyCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudyCustomerService {
    private final StudyCustomerRepository studyCustomerRepository;

    public void setCustomer(CustomerRequest request) {
        StudyCustomer addItem = new StudyCustomer();
        addItem.setCustomerName(request.getCustomerName());
        addItem.setCustomerPhone(request.getCustomerPhone());
        addItem.setUseTime(request.getUseTime());
        addItem.setTimeStart(LocalDateTime.now());
        addItem.setTimeOut(LocalDateTime.now().plusHours(request.getUseTime()));

        studyCustomerRepository.save(addItem);
    }

    public List<CustomerItem> getCustomers() {
        List<StudyCustomer> originData = studyCustomerRepository.findAll();
        List<CustomerItem> result = new LinkedList<>();

        for (StudyCustomer item : originData) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerInfo(item.getCustomerName() + "/" + item.getCustomerPhone());
            addItem.setUseTime(item.getUseTime());
            addItem.setUseTimeFullText(item.getTimeStart() + "~" + item.getTimeOut());
            addItem.setIsEnd(item.getTimeEnd() == null ? false : true);
            addItem.setTimeEnd(item.getTimeEnd());

            result.add(addItem);
        }
        return result;
    }

    public void putUseTime(long id, CustomerUseTimeUpdateRequest request) {
        StudyCustomer originData = studyCustomerRepository.findById(id).orElseThrow();
        originData.setUseTime(request.getUseTime());
        originData.setTimeOut(originData.getTimeStart().plusHours(request.getUseTime()));

        studyCustomerRepository.save(originData);
    }

    public void putTimeEnd(long id) {
        StudyCustomer originData = studyCustomerRepository.findById(id).orElseThrow();
        originData.setTimeEnd(LocalDateTime.now());

        studyCustomerRepository.save(originData);
    }

    public void delCustomer(long id) {
        studyCustomerRepository.deleteById(id);
    }
}
