package com.seop.studycafemanager1.controller;

import com.seop.studycafemanager1.model.CustomerItem;
import com.seop.studycafemanager1.model.CustomerRequest;
import com.seop.studycafemanager1.model.CustomerUseTimeUpdateRequest;
import com.seop.studycafemanager1.service.StudyCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class StudyCustomerController {
    private final StudyCustomerService studyCustomerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        studyCustomerService.setCustomer(request);
        return "ok";
    }

    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = studyCustomerService.getCustomers();
        return result;
    }

    @PutMapping("/use-time/id/{id}")
    public String putUseTime(@PathVariable long id, @RequestBody @Valid CustomerUseTimeUpdateRequest request) {
        studyCustomerService.putUseTime(id, request);
        return "ok";
    }

    @PutMapping("/end-time/id/{id}")
    public String putTimeEnd(@PathVariable long id) {
        studyCustomerService.putTimeEnd(id);
        return "ok";
    }

    @DeleteMapping("/sign-out/id/{id}")
    public String delCustomer(@PathVariable long id) {
        studyCustomerService.delCustomer(id);
        return "ok";
    }
}
