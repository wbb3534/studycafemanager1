package com.seop.studycafemanager1.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Setter
@Getter
public class CustomerUseTimeUpdateRequest {
    @NotNull
    @Min(value = 1)
    @Max(value = 24)
    private Integer useTime;
}
