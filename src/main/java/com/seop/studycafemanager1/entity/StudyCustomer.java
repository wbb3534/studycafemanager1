package com.seop.studycafemanager1.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class StudyCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 20)
    private String customerPhone;
    @Column(nullable = false)
    private Integer useTime;
    @Column(nullable = false)
    private LocalDateTime timeStart; // 입실시간
    private LocalDateTime timeEnd; // 퇴실시간
    @Column(nullable = false)
    private LocalDateTime timeOut; // 시간끝나는시간

}
