package com.seop.studycafemanager1.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 2, max = 20)
    private String customerPhone;
    @NotNull
    @Min(value = 1)
    @Max(value = 24)
    private Integer useTime;
}
