package com.seop.studycafemanager1.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerItem {
    private Long id;
    private String customerInfo;
    private Integer useTime;
    private String useTimeFullText;
    private Boolean isEnd;
    private LocalDateTime timeEnd;
}
